import com.beust.jcommander.*;

public class Jcom {

	@Parameter (names = "-csv", description = "CSV File Name", required = true)
	private String fileName;

	@Parameter (names = "-cost", description = "Total Fixed Cost, Integer Value", required = true)
		private Integer totalCost;

}
