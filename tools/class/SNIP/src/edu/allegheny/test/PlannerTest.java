package edu.allegheny.test;

import static org.junit.Assert.assertEquals;
import org.junit.*;
import java.util.*;

import edu.allegheny.snip.SNIP;

public class PlannerTest {
    SNIP obj = new SNIP();

  @Test
  public void test_getRemainingCash() {
    ArrayList<String> s = new ArrayList<String>();
    ArrayList<Integer> c = new ArrayList<Integer>();

    s.add("c");
    s.add("b");
    s.add("a");
    c.add(3);
    c.add(6);
    c.add(2);

    int a = 1;
    assertEquals(a, obj.getRemainingCash(c, s, 10));
  }

  @Test
  public void test_getTotalCost() {
    ArrayList<String> s = new ArrayList<String>();
    ArrayList<Integer> c = new ArrayList<Integer>();

    s.add("c");
    s.add("b");
    s.add("a");
    c.add(3);
    c.add(6);
    c.add(2);

    int a = 9;
    assertEquals(a, obj.getTotalCost(c, s, 10));
  }

  @Test
  public void test_reqList() {
    ArrayList<String> a = new ArrayList<String>();
    ArrayList<String> s = new ArrayList<String>();
    ArrayList<Integer> c = new ArrayList<Integer>();

    s.add("c");
    s.add("b");
    s.add("a");
    c.add(3);
    c.add(7);
    c.add(2);

    a.add("c");
    a.add("b");
    assertEquals(a, obj.reqList(c, s, 10));
  }

  @Test
  public void test_getCostList() {
    ArrayList<String> s = new ArrayList<String>();
    ArrayList<Integer> a = new ArrayList<Integer>();
    HashMap<String, List<String>> og = new HashMap<String, List<String>>();
    List<String> one = new ArrayList<String>();
    List<String> two = new ArrayList<String>();

    s.add("b");
    s.add("a");
    one.add("1");
    one.add("2");
    two.add("3");
    two.add("4");
    og.put("a",one);
    og.put("b",two);

    a.add(3);
    a.add(1);
    assertEquals(a, obj.getCostList(s,og));
  }

  @Test
  public void test_getRatioKeys() {
    ArrayList<String> a = new ArrayList<String>();
    HashMap<String, Double> r = new HashMap<String, Double>();

    r.put("b",2.2);
    r.put("c",3.3);

    a.add("b");
    a.add("c");
    assertEquals(a, obj.getRatioKeys(r));
  }

  @Test
  public void test_sortRatio() {
    HashMap<String, Double> m = new HashMap<String, Double>();
    HashMap<String, Double> r = new HashMap<String, Double>();

    m.put("a",5.0);
    m.put("b",1.5);
    m.put("c",2.3);
    m.put("d",3.7);

    r.put("a",5.0);
    r.put("d",3.7);
    r.put("c",2.3);
    r.put("b",1.5);
    assertEquals(r, obj.sortRatio(m));
  }

  @Test
  public void test_calculateRatio() {
    HashMap<String, List<String>> m = new HashMap<String, List<String>>();
    HashMap<String, Double> r = new HashMap<String, Double>();
    List<String> one = new ArrayList<String>();
    List<String> two = new ArrayList<String>();
    one.add("1");
    one.add("2");
    two.add("2");
    two.add("6");

    m.put("a",one);
    m.put("b",two);

    r.put("a",2.0);
    r.put("b",3.0);
    assertEquals(r, obj.calculateRatio(m));
  }
}
