package edu.allegheny.snip;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.JCommander;

public class Arguments {
    @Parameter(names = "-debug", description = "Debug mode")
    private boolean debug;

    @Parameter(names =  "--help", description = "This help menu")
    private boolean help;

    @Parameter(names = {"-file", "-f"}, description = "Input file", required = true)
    private String file;

    @Parameter(names = {"-workingcost", "-wc", "-w"}, description = "Working Cost", required = true)
    private int workingCost;

    @Parameter(names = {"-remainingcash", "-rc", "-cash"}, description = "Display remaining cash after implementing requirements")
    private boolean remainingCash;

    @Parameter(names = {"-totalcost", "-tc", "-cost"}, description = "Total cost of implementation")
    private boolean totalCost;

    public boolean getOutput() {
        return debug;
    }

    public boolean getHelp() {
        return help;
    }

    public String getFile() {
        return file;
    }

    public int getWorkingCost() {
        return workingCost;
    }

    public boolean getRemainingCash() {
        return remainingCash;
    }

    public boolean getTotalCost() {
        return totalCost;
    }
}
