/*****************************************************************************
 * @author Colton McCurdy
 * Due: October 23rd, 2015
 * Laboratory Assignment Six
 * Purpose: Next Release Planner
 *
 * Sample Run:
 * java edu.allegheny.snip.SNIP -f "data/sample.csv" -w 1000000
 *
 * [1, 5, 2, 3, 4]
 */

package edu.allegheny.snip;

import java.util.*;
import java.io.*;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.JCommander;

public class SNIP {

  /* Reads in a specified input file passed in by the user. This method
   * parses the input file and then creates a HashMap containing a key of requirement
   * number, and a value of a List which are attributes of each requirement---
   * cost and benefit.
   */
    public static HashMap<String, List<String>> readCsv(String filename) {

        BufferedReader br = null;
        String strLine = "";
        HashMap<String, List<String>> data = new HashMap<String, List<String>>();
        String csvSplit = ",";

        try {

            br = new BufferedReader(new FileReader(filename));
            String headerLine = br.readLine();

            while ((strLine = br.readLine()) != null) {
              String[] lineArr = strLine.split(",");
              List<String> valSet = new ArrayList<String>();
              valSet.add(lineArr[1]);
              valSet.add(lineArr[2]);
              data.put(lineArr[0], valSet); // use comma as separator
            }

        } catch (FileNotFoundException e) {
            System.err.println("Unable to find the file: " + filename);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Unable to read the file: " + filename);
            System.exit(1);
        }

            return data;
    }

    // take in a Map with String requirement and list containing cost and benefit
    // and from the cost and benefit calulate the benefit-cost ratio
    public static HashMap<String, Double> calculateRatio(HashMap<String, List<String>> data) {
        HashMap<String, Double> unorderedList = new HashMap<String, Double>();

        for (Map.Entry<String, List<String>> entry : data.entrySet()) {
          String key = entry.getKey();
          List<String> value = entry.getValue();

          Double valCost = Double.parseDouble(value.get(0));
          Double valBenefit = Double.parseDouble(value.get(1));

          unorderedList.put(key, valBenefit / valCost);
        }
      return unorderedList;
    }

    // mainly broke this up to test accuracy
    // this being seperate will make testing easier
    // a lot of this was provided by the following resource
    // http://www.mkyong.com/java/how-to-sort-a-map-in-java/
    // and modified to accomidate for Double values instead of Integers
    public static HashMap<String, Double> sortRatio(HashMap<String, Double> data) {
      // Convert Map to List
      List<Map.Entry<String, Double>> list =
      new LinkedList<Map.Entry<String, Double>>(data.entrySet());

      // Sort list with comparator, to compare the Map values
      Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
         public int compare(Map.Entry<String, Double> o1,
                            Map.Entry<String, Double> o2) {
           return (o2.getValue()).compareTo(o1.getValue());
           }
      });

      // Convert sorted map back to a Map
      HashMap<String, Double> sortedMap = new LinkedHashMap<String, Double>();

          for (Iterator<Map.Entry<String, Double>> it = list.iterator(); it.hasNext();) {
              Map.Entry<String, Double> entry = it.next();
              sortedMap.put(entry.getKey(), entry.getValue());
          }
        return sortedMap;
      }

    // this method takes the sorted ratio and strips the requirement associated
    // with each ratio
    public static ArrayList<String> getRatioKeys(HashMap<String, Double> r) {
        ArrayList<String> s = new ArrayList<String>();

        for (Map.Entry<String, Double> entry : r.entrySet()) {
          String key = entry.getKey();
          s.add(key);
        }

      return s;
    }

   /* getCostList takes the requirement assiociated with the ratios of benefit/cost
    * and the original data HashMap
    * and creates an array list in order of the costs of each requirement
    * so that we can stay under budget
    */
    public static ArrayList<Integer> getCostList(ArrayList<String> s, HashMap<String, List<String>> org) {
      ArrayList<Integer> c = new ArrayList<Integer>();

        /* for (Map.Entry<String, Double> entry : sorted.entrySet()) { */
      for (String i : s) {
          /* String key = s.get(i); */
          List<String> costKey = org.get(i);
          int cost = Integer.parseInt(costKey.get(0));
          c.add(cost);

        }
        return c;
    }

    /* The final list of requirements in order from right to left.
     * It takes a costmap containing the cost of each element and a working cost
     * that is cutoff for spending on the sum of the implemented requirements
     */
    public static ArrayList<String> reqList(ArrayList<Integer> c, ArrayList<String> s, int w) {
      ArrayList<String> reqList = new ArrayList<String>();
      int temp = 0;

          for (int i : c) {
            if (temp+i <= w) {
              temp += i;
              reqList.add(s.get(c.indexOf(i)));
            }
          }

      return reqList;
    }

    // optional argument where user may want to see the total cost spent on
    // implementing the requirements since all of their working cost may not be
    // utilized
    public static int getRemainingCash(ArrayList<Integer> c, ArrayList<String> s, int w) {
      int temp = 0;

          for (int i : c) {
            if (temp+i <= w) {
              temp += i;
            }
          }

      return w-temp;
    }

    // optional argument where user may want to see the total cost spent on
    // implementing the requirements since all of their working cost may not be
    // utilized
    public static int getTotalCost(ArrayList<Integer> c, ArrayList<String> s, int w) {
      int temp = 0;

          for (int i : c) {
            if (temp+i <= w) {
              temp += i;
            }
          }

      return temp;
    }

    /* ----------------MAIN------------------
     * In main we use JCommander for easily and uniformly parsing command-line
     * arguments. Also in main we call all of the necessary methods to get
     * to the final requirement list that should be implemented.
     */
    public static void main(String[] args) {

      // JCommander
        Arguments params = new Arguments();
        JCommander cmd = new JCommander(params);

        try {
            cmd.parse(args);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            cmd.usage();
            System.exit(1);
        }

        if (params.getHelp() == true) {
            cmd.usage();
        }

          String filename = params.getFile(); // String filename passed as arg
          int workingCost = params.getWorkingCost(); // Integer working cost

        // All of the necessary data structures
        if (params.getOutput() == true) {
          HashMap<String,List<String>> data = readCsv(filename); // data from file
          HashMap<String,Double> unorderedList = calculateRatio(data); // parse data and calculate ben/cost
          HashMap<String,Double> orderedList = sortRatio(unorderedList); // sort ratios
          ArrayList<String> keyList = getRatioKeys(orderedList); // get keys of sorted ratios
          ArrayList<Integer> costList = getCostList(keyList, data); // get cost of each element in sorted ratios
          ArrayList<String> reqList = reqList(costList, keyList, workingCost); // the final ordered list

          System.out.println("Entered Data: \n" + data);
          System.out.println("\nUnordered Ratio List \n" + unorderedList);
          System.out.println("\nOrdered Ratio List \n" + orderedList);
          System.out.println("\nSorted Requirement List without cost \n" + keyList);
          System.out.println("\nCost Map \n" + costList);
          System.out.println("\nIn-Order ((high) left -> right (low)) Requirement List \n" + reqList + "\n");

          if (params.getRemainingCash() == true) {
            int remainingCash = getRemainingCash(costList, keyList, workingCost);
            System.out.println("The Remaining Cash After Implementation: $" + remainingCash);
          }

          if (params.getTotalCost() == true) {
            int totalCost = getTotalCost(costList, keyList, workingCost);
            System.out.println("The Total Cost of Implementation: $" + totalCost + "\n");
          }
        } else {

          HashMap<String,List<String>> data = readCsv(filename); // data from file
          HashMap<String,Double> unorderedList = calculateRatio(data); // parse data and calculate ben/cost
          HashMap<String,Double> orderedList = sortRatio(unorderedList); // sort ratios
          ArrayList<String> keyList = getRatioKeys(orderedList); // get keys of sorted ratios
          ArrayList<Integer> costList = getCostList(keyList, data); // get cost of each element in sorted ratios
          ArrayList<String> reqList = reqList(costList, keyList, workingCost); // the final ordered list

          System.out.println("\n"+reqList);
        }
    }
}
