# S.N.I.P.
## Simple. New. Improved. release Planner.
______

## Participated in Agile software Development?
______

During the creation of S.N.I.P. we as a team decided it would be best to have Agile Software Development for allowing us to get out software out to customers as fast as possible, as advanced as possible, and constantly upgrading. At the beginning we discussed the potential of creating a system that would be "smarter" and give the user of the program only the best choices for releasing requirements of a software system. That being said, the possibility for creating this system was quashed due to our time constraints. For this reason, we as a team designed and implemented a system that gives the user of the program all the choices arranged in order of the ratio of benefit to cost for the requirement. It is our goal to stay true to our commitment, and releasing the more advanced system in the near-future.

## S.N.I.P. Overview
______

The purpose of this program is to iterate through a file containing requirements with their associated costs and benefits separated by a comma, and one requirement per a line. This system provides a relatively quick output (depending on the number of requirements), to inform the user of the program which requirements that should be released. In addition to reading in the name of the file through the terminal, the user also has to include maximum cost that they can afford for the program. This allows the program to order the list of requirements into getting the most benefit out of all of the requirements as possible. 

## Preparation Overview
______
_The following bulleted points will be covered in more detail below._
+ Launch Terminal
+ Install S.N.I.P.
+ Set Classpath
+ Usage

### Launching Terminal
______
Our system, in its current state, requires you to use the
Terminal Application (OSX, Linux).
Launching the Terminal will put you in your home directory.
To make installing and using our system as simple for you as possible,
we will assume that it is okay that you install your system in your home
directory. We will not provide you with the shell commands necessary
to navigate Terminal as this is not necessary in the usage of our
system.

After launching either Terminal,
you may proceed with the rest of this tutorial on using `SNIP`.

### Installation
______
The easiest way to get our system is to clone
the repository to your local machine. If you are familiar with git and
cloning repositories use the following command. Otherwise, if you are
interested in learning git, please refer to this
[link](https://try.github.io/levels/1/challenges/1) for a wonderful
git tutorial that explains the basics of using git as a tool for version
control.

```
# clone S.N.I.P. to your local machine using git
git clone git@bitbucket.org:mccurdyc/cs280f2015-lab6-team3.git
```

### Set classpath

First you will need to change into the `SNIP` directory using the
following command

```
cd SNIP
```

Then, export your Classpath using the following command

```
export CLASSPATH=$CLASSPATH:bin:lib/jcommander-1.48.jar:lib/junit-4.12.jar:lib/hamcrest-core-1.3.jar:.
```

### Usage
______

[![asciicast](https://asciinema.org/a/7j7c1ldc1grfx6e1ktf49wr1a.png)](https://asciinema.org/a/7j7c1ldc1grfx6e1ktf49wr1a)

Print the help menu displaying all of the excepted command-line
arguments
```
# display accepted command-line args
java edu.allegheny.snip.SNIP

# or
java edu.allegheny.snip.SNIP --help
```

All of the supported and required parameters can be found in the help menu
```
The following options are required: -workingcost, -wc, -w -file, -f
Usage: <main class> [options]
  Options:
    --help
       This help menu
       Default: false
    -debug
       Debug mode
       Default: false
  * -file, -f
       Input file
    -remainingcash, -rc, -cash
       Display remaining cash after implementing requirements
       Default: false
    -totalcost, -tc, -cost
       Total cost of implementation
       Default: false
  * -workingcost, -wc, -w
       Working Cost
       Default: 0
```

To build and compile our system
```
ant compile
```

To run `SNIP` without debug mode
This will return just a list of requirements in order from left -> right
```
java edu.allegheny.snip.SNIP -file "path/to/file" -w <int working-cost>
```

To run `SNIP` in debug mode
This returns basically the process in which we go about creating the
final list. This mode also supports two optional parameters
```
    -remainingcash, -rc, -cash
       Display remaining cash after implementing requirements
       Default: false
    -totalcost, -tc, -cost
       Total cost of implementation
       Default: false
```

```
java edu.allegheny.snip.SNIP -file "path/to/file" -w <int working-cost> -debug -cash -cost
```

# Requirements
______
_For the full list of requirements see_
[requirements](http://www.cs.allegheny.edu/sites/gkapfham/teaching/cs280F2015/provide/labs/lab06/cs280F2015_lab06.pdf).

# Feedback
______
If you like it, hate it, or have some ideas for new features, let us
know! Our individual Bitbucket accounts are provided below!

# Contributors
______

+ Colton McCurdy ([mccurdyc](https://bitbucket.org/mccurdyc/))
+ Trevor Kroon ([kroont](https://bitbucket.org/kroont/))
+ Francis Craft ([craftfrancis](https://bitbucket.org/craftfrancis/))

# License
______

GNU General Public License v3.0

Please refer to the 
[License](https://bitbucket.org/mccurdyc/cs280f2015-lab6-team3/src/365801f96a066a9f43043c16d2ce01fe3876e0a1/SNIP/LICENSE.txt?at=master&fileviewer=file-view-default), for the details regarding the GNU General Public License
v3.0.
