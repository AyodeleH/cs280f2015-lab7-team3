# Design Document
## Greedy Algorithm

Planner.java was designed with the Greedy algorithm in mind. The high level setup is that the system will read in the necessary inputs, a csv file and the working Cost, from the user. Then for output, it will return the list of the best ratio elements that fit the cost specified.

Example:

*java Planner Input.csv 150*

*Example OutPut*

For a more indepth description of the design of Planner.java included is a flow chart that explicitly describes the process that the program follows. 

- Starting in the top on the left is Planner. This represents program running. This has one input, which comes from Arguments

- Arguments is a class that Planner implements that allows for better runtime input parsing. It has two inputs, run time input and file

- Run time input is what you, the user, specify as you initiate the programs execution

- File is the csv file you specify as you run the program

- Once Planner has taken the parsed information from Arguments it enters the main method

- The main method is the section of the code during which the program begins its interpretation of the input. The main method follows the progression of getRatio --> sort --> returnInOrder --> ordered list. Along side getRatio, sort, and returnInOrder along a dotted line is a box that explains how each method runs. In these methods, R refers to the set of requirements from the csv file, C refers to the set of cost of each requirement, and B refers to the set of benefits of each requirement.

- getRatio, the first method, computes the ration between B<sub>i</sub> and c<sub>i</sub>. This means for every element in B and C a ratio is established.

-sort takes those ratios and sorts the sets in descending order starting from the highest to lowest.

-returnInOrder removes the elements of the sorted list one at a time. However it does not return all of them. Each time an item is returned, the sum of the cost of all the items so far returned is tallied. If the next item's cost added would exceed the working cost, as specified by you in the run time arguments, it is not added to the list. That ordered list is then returned to you.
