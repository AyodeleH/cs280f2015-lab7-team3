# Roles Document for Team 3 Laboratory 6

## Colton McCurdy
Mr. McCurdy was the Chief Programmer in this lab and was in charge of
not only designing the system, but also implementing the system. This
was due to the fact that his colleague, Trevor Kroon, would not be able
to finish implementing the system in a timely manner, because it was too
advanced for him. Mr. McCurdy also did the system testing to make sure
that the team had made the correct program.

## Francis Craft
Mr. Craft was the Maintainer and therefore in charge of documenting a
majority of the necessary documents. He was also the leader in writing
the presentation and making sure it was aesthetically pleasing to the
viewers.

## Trevor Kroon
Mr. Kroon was the Assistant Programmer in this lab, and while he didn't
do the implementation of the program as planned, he assisted where
appropriate at the instruction of Mr. McCurdy.
