# Team Three's Designated Roles Document

## Colton McCurdy
For this assignment, Colton was the designated tool user. He was in
charge of familiarizing his teammate with both of the tools. He ran
JDepend and JavaNCSS on two repositories from previous laboratory
assignments from this course. Additionally, Colton chose to run the
JDepend and JavaNCSS on two open-source projects, those being JUnit and
Simpletask for Android. He collected the results from the runs of both
tools on each of the aforementioned repositories.

After collecting the results from the tools, Colton was in charge of
analyzing those results and forming conclusions about what a
well-designed programming systems product entails in terms of
quantitative data provided from these tools and conversely, what makes a
programming systems product poorly designed. Colton also co-authored the
presentation displaying and discussing our group's analysis of the
aforementioned tools.

## Ayodele Hamilton
For this assignment, Ayodele was in charge of defining the metrics that
these tools provided a user with when analyzing the design of a
programming systems product. Ayodele clearly defined the meaning of
JDepend's design quality metrics---extensibility, reusability and
maintainability. Ayodele was also the author of a document clearly
explaining the meaning of the metrics calculated by JavaNCSS.

Finally, Ayodele co-author a presentation displaying and discussing our
group's analysis of the aforementioned tools.
