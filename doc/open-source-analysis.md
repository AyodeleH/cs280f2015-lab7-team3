# Analysis of the Design of Open-Source Systems
## Overview

We chose to analyze the design of two open-source systems.
The two open-source tools that we analyzed were JUnit and
Simpletask-android. We used two tools to analyze the design
of these systems, JDepend which allowed us to compare the number of
afferent versus efferent couplings and how this affected the overall
stability of a program. The second tool that we used to analyze the
design of our systems was JavaNCSS. JavaNCSS calculates a number of
metrics, for different levels of a system (e.g., function level,
class level, package level), but we chose to focus on three metrics.
Those metrics were Average NCSS, Total NCSS, and Average CCN. We chose
to analyze these metrics for the class and function levels. More detail
in regards to our findings will be provided in the following section.

## Analysis of Findings
### JDepend
As aforementioned, the first tool that we used to measure the quality of
the design for our systems was JDepend. We used JDepend to find the
number of afferent and efferent couplings. From these metrics, we were
able to conclude that the more afferent couplings and fewer efferent
couplings resulted in a more stable system---see Figure 1 below. In
Figure 1, we analyzed Simpletask-android and found that the more afferent
couplings that were contained, the more stable the class tended to be.

![Figure 1: Simpletask-android Afferent v. Efferent](../results/imgs/simpletask-ca-ce.pdf)

In Figure 2, we analyzed the other open-source tool, JUnit, and found that
the more afferent couplings that were contained, the more stable the class
tended to be. This hypothesis is not as clearly supported for JUnit
since there are no completely stable couplings.

![Figure 2: JUnit Afferent v. Efferent](../results/imgs/junit-ca-ce.pdf)

### JavaNCSS
The second tool used to measure the quality of the design for our
systems was JavaNCSS. JavaNCSS calculated three metrics for both class
level and function level used when analyzing our system. These metrics
were the average NCSS, average CCN, and total NCSS. At a function level,
the open-source systems tended to have less non commented source
statements than the our systems.
For the size of the systems, they still managed to have a low cyclomatic
complexity---a lower-is-better (LIB)
metric---JUnit almost obtained a CCN of 1.0 at a function level.
Finally, in our class analysis document, we
hypothesized that the number of total non commented source statements this 
could affect the aforementioned
metrics discussed in this document. We hypothesize that since these open-source
have a high number of non commented source statements and also a high
number of classes, that most of the classes in their system are simple
get and set classes with CCNs of 1.0, reducing the average CCN of the
entire system. Additionally, since these tools have a lot of get and set
classes, they also reduce the average non commented source statements
for objects. Again, refer to Figure 3, for a visualization of these metrics.

![Figure 3: JavaNCSS Function-Level](../results/imgs/functions.pdf)

At an object level, both open-source tools possessed relatively similar
scores for both the average non commented source statements---both
around 20---and average number of functions per object---averaged to be
about 3.7 functions per object.
These numbers in comparison to our systems were good, allowing us to
conclude that these open-source tools are designed well according to
JavaNCSS. Again, refer to Figure 4 to see the results from JavaNCSS at
an object level.

![Figure 4: JavaNCSS Object-Level](../results/imgs/object-png.png)

## Conclusion

In conclusion, when considering the design quality metrics provided from
JDepend and JavaNCSS, the open-source are well-designed. JUnit,
according to both JavaNCSS and JDepend has a higher quality design.
While Simpletask-android did not have as high quality of a design as
JUnit according to these tools, it still possessed a high quality design
for the size.
