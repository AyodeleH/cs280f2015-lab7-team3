# Analysis of the Design of Class Laboratory Assignments

## Overview
We chose to analyze the design of two previous laboratory assignments.
The assignments that we chose were both from laboratory assignment six,
creating a next-release planner. We used two tools to analyze the design
of our systems, JDepend which allowed us to compare the number of
afferent versus efferent couplings and how this affected the overall
stability of a program. The second tool that we used to analyze the
design of our systems was JavaNCSS. JavaNCSS calculates a number of
metrics, for different levels of a system (e.g., function level,
class level, package level), but we chose to focus on three metrics.
Those metrics were Average NCSS, Total NCSS, and Average CCN. We chose
to analyze these metrics for the class and function levels. More detail
in regards to our findings will be provided in the following section.

## Analysis of Findings
### JDepend
As aforementioned, the first tool that we used to measure the quality of
the design for our systems was JDepend. We used JDepend to find the
number of afferent and efferent couplings. From these metrics, we were
able to conclude that the more afferent couplings and fewer efferent
couplings results in a more stable system---see Figure 1 below. In
Figure 1, we analyzed SNIP and found that the more afferent couplings
that were contained, the more stable the class tended to be.

![Figure 1: SNIP: Afferent v. Efferent](../results/imgs/snip-ca-ce.pdf)

In Figure 2, we analyzed the other group's Lab6 assignment and found that
the more afferent couplings that were contained, the more stable the class
tended to be. This hypothesis is not as clearly supported for Lab6.

![Figure 2: Lab6: Afferent v. Efferent](../results/imgs/lab6-ca-ce.pdf)

### JavaNCSS
The second tool used to measure the quality of the design for our
systems was JavaNCSS. JavaNCSS calculated three metrics for both class
level and function level used when analyzing our system. These metrics
were the average NCSS, average CCN, and total NCSS. At a function level,
our systems tended to have more non commented source statements than the
open source tools that we compared them to. Also, for the size of our
systems, they had high cyclomatic complexity---a lower-is-better (LIB)
metric---both were around 1.7 at a function level. Finally, our systems
were small in terms of total non commented source statements when
compared to the open source tools. This was to be expected. However, we
hypothesize that this metric possibly could affect the aforementioned
metrics, we will go into more detail about how the total NCSS could
affect the metrics for open source tools in open-source-analysis.md.
Refer to Figure 3, for a visualization of these metrics.

![Figure 3: JavaNCSS Function-Level](../results/imgs/functions.pdf)

At an object level, the SNIP tool displayed lower quality design,
tending to have a higher number of non commented source statements per
object and also a higher number of functions per object. Both of
these metrics are lower-is-better metrics, higher numbers
infer that the functions or object are cluttered or over-populated with
non commented source statements. The average NCSS for a well-designed
systems was anywhere below 30 non commented source statements and the
SNIP tool had over 50 non commented source statement, displaying low
design quality characteristics. Similarly, well-designed open-source
tools tend to have 4 or fewer functions per class, where the SNIP tool
had almost 6 functions per class on average. For a visualization of the
metrics aforementioned at an object level, see Figure 4.

![Figure 4: JavaNCSS Object-Level](../results/imgs/object-png.png)

## Conclusion

In conclusion, when considering the design quality metrics provided from
JDepend and JavaNCSS, SNIP high a low quality design because its
functions and classes tended to have too many non commented source
statements for each. However, SNIP was considered to be stable in terms
of the coupling. On the other hand, Lab6 had a high quality design
according to JavaNCSS, keeping objects and functions to a low number of
non commented source statements. According to JDepend, the stability of
Lab6 was considered to be relatively stable.

